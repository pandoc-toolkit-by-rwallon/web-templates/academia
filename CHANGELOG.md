# Changelog

This file describes the evolution of the *Academia* Web template for Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.
