---
# Metadata of the web page.
language:
description:
keywords:
  -
years:

# Translations.
available-languages:
  - name:
    flag:
    url:

# Webpage appearance.
header-image:
contact-title:
map-title:

# Informations about the owner of the web page.
name:
position:
email:
photo:
office:
address:
  -
institute:
  - name:
    url:
    logo:

# Biography.
biography-only:
biography-title:
biography: |

# CV.
cv-only:
cv-title:
cv-description: |

cv:
  experiences:
    title:
    list:
      - date:
        description:
  students:
    title:
    list:
      - date:
        description:
  committees:
    title:
    list:
      - date:
        description:
  grants:
    title:
    list:
      - date:
        description:
  education:
    title:
    list:
      - date:
        description:
  skills:
    title:
    list:
      - category:
        description:
  leasures:
    title:
    list:
      -

# Teaching.
teaching-only:
teaching-title:
teaching-description: |

teaching:
  - academic-year:
    active:
    where:
    description:
      - name:
        hours:

# Research.
research-only:
research-title:
research-description: |

publications:
  - category:
    papers:
      - id:
        title:
        authors:
        description:
        date:
        abstract: |

        bibtex: |
          ```
          ```
        goto:
        url:
      - title:
        description:
        date:
        url:

# Software.
software-only:
software-title:
software-description: |

software:
  - name:
    description: |

    goto:
    url:
---
