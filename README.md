# *Academia* Web Template for Pandoc

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/web-templates/academia/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/web-templates/academia/commits/master)

## Description

This project provides a template allowing an easy creation of web pages 
for people working in academia with [Pandoc](https://pandoc.org).

This template is distributed under a Creative Commons Attribution 4.0
International License.

[![CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)

## Requirements

To build your web page using this template, [Pandoc](https://pandoc.org) at
least 2.0 have to be installed on your computer.

## Creating your Web Page

To create your website, you need to setup its YAML configuration.
For convenience, we provide a [`metadata.md`](./metadata.md) file in which all
possible settings are specified, so that you can just set them or remove the
ones which do not fit your needs.
Read below for details on how to configure your web page.

> **TODO WRITE DOCUMENTATION**

## Building your Web Page

Suppose that you have written the metadata of your website in a file `input.md`.
Then, to produce the website in a file named `output.html`, execute the
following command:

```bash
pandoc --template academia.pandoc input.md -o output.html
```
